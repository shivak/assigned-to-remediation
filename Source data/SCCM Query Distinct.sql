SELECT DISTINCT
v_GS_COMPUTER_SYSTEM.ResourceID as id_sccm,
v_GS_SYSTEM_ENCLOSURE.SerialNumber0 as serial_number,
v_GS_COMPUTER_SYSTEM.Name0 as ci,
v_GS_COMPUTER_SYSTEM.Model0 as model_sccm,
v_GS_SYSTEM_CONSOLE_USAGE.TopConsoleUser0 as TopConsoleUser_sccm,
v_GS_PROCESSOR.NormSpeed0 as CPUSpeed,
v_GS_OPERATING_SYSTEM.Caption0 as os_sccm,
CAST(v_GS_X86_PC_MEMORY.TotalPhysicalMemory0/1024/1024 as numeric) + 1 as RAM,
SUM(CAST(v_GS_PHYSICAL_DISK.Size0/1024/1024/1024 as numeric)) as HDDSize,
v_GS_SYSTEM_ENCLOSURE.ChassisTypes0 as chassistype,
FORMAT(WorkstationStatus_DATA.LastHWScan, 'MM-yyyy') as last_hw_scan_sccm,
WorkstationStatus_DATA.TimeKey as timekey_sccm,
v_R_System.Last_Logon_Timestamp0 as lastlogon_datetime_sccm,
v_R_System.User_Name0 as lastlogon_username_sccm


FROM v_GS_COMPUTER_SYSTEM
LEFT JOIN WorkstationStatus_DATA ON v_GS_COMPUTER_SYSTEM.ResourceID = WorkstationStatus_DATA.MachineID
LEFT JOIN v_GS_SYSTEM ON v_GS_COMPUTER_SYSTEM.ResourceID = v_GS_SYSTEM.ResourceID
LEFT JOIN v_GS_PC_BIOS ON v_GS_COMPUTER_SYSTEM.ResourceID = v_GS_PC_BIOS.ResourceID
LEFT JOIN v_GS_OPERATING_SYSTEM ON v_GS_COMPUTER_SYSTEM.ResourceID = v_GS_OPERATING_SYSTEM.ResourceID
LEFT JOIN v_GS_COMPUTER_SYSTEM_PRODUCT ON v_GS_COMPUTER_SYSTEM.ResourceID = v_GS_COMPUTER_SYSTEM_PRODUCT.ResourceID
LEFT JOIN v_GS_SYSTEM_ENCLOSURE ON v_GS_COMPUTER_SYSTEM.ResourceID = v_GS_SYSTEM_ENCLOSURE.ResourceID
LEFT JOIN v_GS_BASEBOARD ON v_GS_COMPUTER_SYSTEM.ResourceID = v_GS_BASEBOARD.ResourceID
LEFT JOIN v_GS_SYSTEM_CONSOLE_USAGE ON v_GS_COMPUTER_SYSTEM.ResourceID = v_GS_SYSTEM_CONSOLE_USAGE.ResourceID
LEFT JOIN v_GS_X86_PC_MEMORY ON v_GS_COMPUTER_SYSTEM.ResourceID = v_GS_X86_PC_MEMORY.ResourceID
LEFT JOIN v_GS_PROCESSOR ON v_GS_COMPUTER_SYSTEM.ResourceID = v_GS_PROCESSOR.ResourceID
LEFT JOIN v_GS_PHYSICAL_DISK ON v_GS_COMPUTER_SYSTEM.ResourceID = v_GS_PHYSICAL_DISK.ResourceID
LEFT JOIN v_UserMachineRelationship ON v_GS_COMPUTER_SYSTEM.ResourceID = v_UserMachineRelationship.MachineResourceID
LEFT JOIN v_R_System on v_GS_COMPUTER_SYSTEM.ResourceID = v_R_System.ResourceID


WHERE v_GS_SYSTEM_ENCLOSURE.ChassisTypes0 NOT LIKE '1'
AND v_GS_COMPUTER_SYSTEM.Name0 LIKE '%-%' 

GROUP BY
    v_GS_COMPUTER_SYSTEM.ResourceID,
    v_GS_SYSTEM_ENCLOSURE.SerialNumber0,
    v_GS_COMPUTER_SYSTEM.Name0,
    v_GS_COMPUTER_SYSTEM.Model0,
    v_GS_SYSTEM_CONSOLE_USAGE.TopConsoleUser0,
    v_GS_PROCESSOR.NormSpeed0,
    v_GS_OPERATING_SYSTEM.Caption0,
    CAST(v_GS_X86_PC_MEMORY.TotalPhysicalMemory0 / 1024 / 1024 AS numeric) + 1,
    v_GS_SYSTEM_ENCLOSURE.ChassisTypes0,
    FORMAT(WorkstationStatus_DATA.LastHWScan, 'MM-yyyy'),
    WorkstationStatus_DATA.TimeKey,
    v_R_System.Last_Logon_Timestamp0,
    v_R_System.User_Name0
